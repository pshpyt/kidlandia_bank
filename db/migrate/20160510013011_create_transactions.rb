class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :user_id
      t.integer :account_id
      t.integer :amount
      t.string :transaction_type
      t.timestamps null: false
    end
  end
end
