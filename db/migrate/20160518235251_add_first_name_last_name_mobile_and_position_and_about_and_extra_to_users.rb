class AddFirstNameLastNameMobileAndPositionAndAboutAndExtraToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :mobile, :string
    add_column :users, :position, :string
    add_column :users, :about, :text
    add_column :users, :extra, :hstore
  end
end
