class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :card_number
      t.string :last_name
      t.string :first_name
      t.string :middle
      t.date :dob
      t.integer :balance

      t.timestamps null: false
    end
  end
end
