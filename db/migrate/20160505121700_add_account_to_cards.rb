class AddAccountToCards < ActiveRecord::Migration
  def change
    add_reference :cards, :account, index: true, foreign_key: true
  end
end
