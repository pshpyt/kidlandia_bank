class AddCardTypeAndCardNumberToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :card_type, :string
    add_column :accounts, :card_number, :string
  end
end
