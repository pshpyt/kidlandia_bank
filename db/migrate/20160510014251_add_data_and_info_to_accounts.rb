class AddDataAndInfoToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :data, :hstore
    add_column :accounts, :info, :string
  end
end
