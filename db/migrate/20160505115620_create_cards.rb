class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :number
      t.string :type
      t.boolean :active
      t.integer :balance

      t.timestamps null: false
    end
  end
end
