json.array!(@cards) do |card|
  json.extract! card, :id, :number, :type, :active, :balance
  json.url card_url(card, format: :json)
end
