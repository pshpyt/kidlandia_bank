class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /transactions
  # GET /transactions.json
  def index
    params[:q] ||= {}
    if params[:q][:created_at_lteq].present?
      params[:q][:created_at_lteq] = params[:q][:created_at_lteq].to_date.end_of_day
    end
    if params[:q][:created_at_gteq].present?
      params[:q][:created_at_gteq] = params[:q][:created_at_gteq].to_date.beginning_of_day
    end  
    @q = Transaction.ransack(params[:q])
      @transactions  = @q.result.includes(:account)
      @total_plus = 0
      @total_minus = 0
      @total_transactions = 0 
      @transactions.find_each do |t|
         @total_transactions += 1 
         case t.transaction_type
          when Account::OPERATIONS.first
           @total_plus += t.amount.to_i    
          when Account::OPERATIONS.last
           @total_minus += t.amount.to_i    
          end
      end  
         @transactions = @transactions.page(params[:page])
  
  
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.create(transaction_params)
    
    
   # @account = Account.find(transaction_params["account_id"])

   # binding.pry
    if @transaction.transaction_type == 'Пополнение'
     @transaction.account.balance = @transaction.account.balance + @transaction.amount.to_i
    else
     @transaction.account.balance = @transaction.account.balance - @transaction.amount.to_i
    end 
    
    respond_to do |format|
      if @transaction.save
        @transaction.account.save
        format.html { redirect_to @transaction.account, notice: 'Транзакция успешно прошла' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to @transaction, notice: 'Transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url, notice: 'Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit! #(:user_id, :account_id, :amount, :transaction_type, :data)
    end
end
