namespace :db do
  desc "Create 100 accounts"
  task :populate => :environment do
      require 'populator'
      require 'faker'
      Account.destroy_all
      Account.populate 100 do |account|
        Faker::Config.locale = :ru
           account.first_name   = Faker::Name.male_first_name
           account.card_number  = "1000#{Faker::Number.number(6)}"
           account.card_type    = ["пластиковая", "бумажная"].sample
           account.middle       = Faker::Name.male_middle_name
           account.last_name    = Faker::Name.male_last_name
           account.balance      = Faker::Number.number(6).to_i
           account.dob          = Faker::Date.between(12.years.ago, 4.years.ago) #=> "Wed, 24 Sep 2014"
           
           #
           #Faker::Config.locale = :en
       #    customer.latin_first_name = Faker::Name.first_name
       #    customer.latin_middle_name = Faker::Name.last_name
       #    customer.latin_last_name = Faker::Name.last_name
      end
      puts "created 100 users"
  end
end
