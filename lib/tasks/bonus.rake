# lib/tasks/db.rake
namespace :bank do
  desc "Calculate bonus"
  task :bonus_calculate => :environment do
  # find all positive transactions for today
    plus_transactions = Transaction.where('created_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day).all
  
    #set  default bonus amount
    bonus_amount = "5"
    if Setting['bonus_amount'] 
      bonus_amount =  Setting['bonus_amount']
    end
      
    accounts = []  
    plus_transactions.each do |t|
      accounts << t.account
    end
    accounts.uniq!
    total_amount    = 0
    total_accounts  = 0

    accounts.each do |account|
      begin
        if account.balance.to_i > 0
          bonus_transaction = Transaction.new(:user_id => account.user.id, :account_id => account.id, :amount => bonus_amount, )
          bonus_transaction.transaction_type = "Пополнение"
          bonus_transaction.save
          total_amount    += bonus_amount.to_i
          total_accounts  += 1
        end
      end
    end
  puts "-----> Finished! Total bonuses: #{total_amount} Total accounts: #{total_accounts}"  
  end
end
