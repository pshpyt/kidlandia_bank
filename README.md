Инструкция
 Проэкт "Kidlandia_bank" выполнены в форме веб-сайта.
Для их использывания нужен один сервер,на котором будет устновлено необходимое ПО для запуска проэктов.
После этого другие компьютеры (клиенты) при помощи веб-браузера могут использовать проэкты через сеть.
На клиентах нужен только веб-браузер Chrome для пользования проэктами. Никакого дополнительного ПО устанавливать не нужно.


1. Системные требования:
   Сервер:
	
```
#!ruby

 RAM 128 мб / 100 мб HDD 
 OS: Windows или Linux (желательно)
 Язык программирования Ruby 2.2.2 
 Веб сервер для для запуска программ   WEBrick / Thin / Nginx with Passanger / Apache
```

   Клиент (компьютер на котором дети будут пользоваться проэктами):
	На клиентах устанавливать проэкты не нужно,так как они будут запускать проэкты при помощи веб-браузера через локальную сеть.
	Все что нужно на клиентах - это веб-браузер Chrome. 


2. Установка необходимиго ПО на примере Ubuntu 12.04
	Для запуска проэктов нужно установить Ruby on Rails with RVM :
https://www.digitalocean.com/community/tutorials/deploying-a-rails-app-on-ubuntu-14-04-with-capistrano-nginx-and-puma	

```
#!python

sudo apt-get update
```

```
#!python

sudo apt-get install curl git-core 
```


```
#!ruby

sudo apt-get install curl 
```

```
#!python

sudo apt-get install libpq-dev
```

```
#!python

sudo apt-get install nodejs
```


```
#!ruby

\curl -L https://get.rvm.io | bash -s stable
```
if fails check output and run command from help block:

```
#!python

 gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
```
After run again:

```
#!python

\curl -L https://get.rvm.io | bash -s stable
```

	
```
#!ruby

source ~/.rvm/scripts/rvm
```

	
```
#!terminal

rvm requirements
```

	
```
#!python

rvm install 1.9.3 
rvm use 1.9.3 --default
gem install rails  -v "3.2.13" 
gem install passenger

```
Setup nginx, passenger with rvm:


```
#!python

rvmsudo passenger-install-nginx-module

```

Also can use this script for init.d service  

```
#!ruby

wget -O init-deb.sh http://library.linode.com/assets/660-init-deb.sh 
sudo mv init-deb.sh /etc/init.d/nginx 
sudo chmod +x /etc/init.d/nginx 
sudo /usr/sbin/update-rc.d -f nginx defaults 
```
 
after that you can control nginx with  

```
#!ruby

sudo /etc/init.d/nginx stop 
sudo /etc/init.d/nginx start 
sudo /etc/init.d/nginx restart
```


To setup nginx working with app which use another ruby version, all you need to add 
  passenger_ruby into nginx.conf. This var can be found by next steps:
 select with rvm new ruby version:

```
#!python

rvm use 2.2.2
```

```
#!python

gem install rails
gem install passenger
```
and after input in terminal next command:

```
#!python

which passenger-config

```
which in results will show something like this:

```
#!python

/home/pasha/.rvm/gems/ruby-2.2.2-p551/bin/passenger-config
```
after add to this string --ruby-command and input into terminal:

```
#!python
/home/pasha/.rvm/gems/ruby-2.2.2-p551/bin/passenger-config --ruby-command
```

After you will get able to get passenger_ruby for required ruby version to insert into nginx.conf. Thats it)

passenger-config was invoked through the following Ruby interpreter:
  

```
#!ruby

Command: /home/pasha/.rvm/gems/ruby-2.2.2-p551/wrappers/ruby
  Version: ruby 2.2.2p551 (2014-11-13 revision 48407) [i686-linux]
  To use in Apache: PassengerRuby /home/pasha/.rvm/gems/ruby-2.2.2-p551/wrappers/ruby
  To use in Nginx : passenger_ruby /home/pasha/.rvm/gems/ruby-2.2.2-p551/wrappers/ruby
  To use with Standalone: /home/pasha/.rvm/gems/ruby-2.2.2-p551/wrappers/ruby /home/pasha/.rvm/gems/ruby-2.2.2-p551/gems/passenger-5.0.30/bin/passenger start
```

 
instert next line into nginx.conf
```
#!python

 To use in Nginx : passenger_ruby /home/pasha/.rvm/gems/ruby-2.2.2-p551/wrappers/ruby
```
example of nginx.conf


```
#!python

http {
    passenger_root /home/pasha/.rvm/gems/ruby-2.2.2/gems/passenger-5.0.30;
    passenger_ruby /home/pasha/.rvm/gems/ruby-2.2.2/wrappers/ruby;

    include       mime.types;
    default_type  application/octet-stream;

server { 
listen 81; 
server_name example.com; 
passenger_enabled on; 
rails_env development; 
root /home/pasha/work/roksolana/stockroom/public;
}


server { 
listen 83; 
server_name example.com; 
passenger_enabled on; 
rails_env development;
root /home/pasha/work/roksolana/trash/driver/public;
passenger_ruby /home/pasha/.rvm/gems/ruby-1.9.3-p551/wrappers/ruby;

# location / {
#        proxy_pass http://127.0.0.1:82;
#        proxy_set_header Host $host;
#    }
}
server { 
listen 82; 
server_name example.com; 
passenger_enabled on; 
rails_env development;
root /home/pasha/work/roksolana/trash/kids/public;
passenger_ruby /home/pasha/.rvm/gems/ruby-1.9.3-p551/wrappers/ruby;


```

Setup PG:


```
#!python

sudo -u postgres psql
postgres=# \password postgres
два раза пароль kidsclub
\q
```

Setup SSH keys:

```
#!python

ssh-keygen -t rsa
```
Once you have entered the Gen Key command, you will get a few more questions:

```
#!python

Enter file in which to save the key (/home/demo/.ssh/id_rsa):
Enter passphrase (empty for no passphrase): 
```
 just press enter.
     
```
#!ruby

bundle exec rake db:create
bundle exec rake db:migrate
bundle exec rake db:seed
```

  После этого можно запускать веб-сервер :
http://code.macournoyer.com/thin/doc/files/README.html	
```
#!python

bundle exec rails server
```






	
  Полезные ссылки:
	http://ryanbigg.com/2010/12/ubuntu-ruby-rvm-rails-and-you/
	https://www.digitalocean.com/community/articles/how-to-install-ruby-on-rails-on-ubuntu-12-04-lts-precise-pangolin-with-rvm


Cron jobs in Ruby (https://github.com/javan/whenever)

You can list installed cron jobs using
```
#!terminal

 crontab -l
```

После устоновки на новом сервере нужно выполнить следующую команду для создания Cron Jobs:

```
#!ruby

$bundle exec  whenever --update-crontab
```

Сделать .dump базы данных:  
```
#!ruby

bundle exec rake db:dump                            # Dumps the database to db/backups/
```


```
#!ruby

bundle exec rake bank:bonus_calculate               # Calculate bonus
```


Загрузить базу данных с .dump файла:
```
#!ruby

bundle exec rake db:restore  # Restores the database dump: bundle exec rake db:restore dump_name=..

```
			Инструкция пользования админкой проэктов
			 -------------------------------------



ля того ,чтобы зайти в админку нужно  перейти по ссылке "/admin" (например http://162.243.96.214:3000/admin)
Для создания нового пользователя используйте "/users/sign_up"  (например http://162.243.96.214:3000/users/sign_up)